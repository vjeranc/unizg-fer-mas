# Homeworks

## hw1 (dz1)

DCT + quantization

`vjeran_crnjak_dz1.c` after compiling with `gcc -std=c99 -O3 -ffast-math vjeran_crnjak_dz1.c -o exe -lm` can be called:

```
./exe dz1/lenna.ppm 0 out.txt
# out.txt will contain coefficients of 8x8 block at index 0
./exe dz1/lenna.ppm 0 lenna.ppm ANYTHING ELSE
# this will do a full dct then idct and store the .ppm image with artifacts
# that were a result of quantization
```

~~Added OpenMP pragma on general DCT loop.~~

Compile `gcc -std=c99 -O3 -fopenmp -ffast-math vjeran_crnjak_dz1.c -o exe -lm`:

```
./exe dz1/themaster.ppm 0 themaster.ppm ANYTHING ELSE
```

~~Marvel at the easy parallelism.~~

Removed parallelism, seems to be slow for even `themaster.ppm`.
Reason why it is slow is that function pointers of `mul` and `correct` are
pointers and not inlined, if one writes a non-general function (copying the gdct and just using the name) it'll be faster and parallel gains will be visible.
They aren't inlined in case OpenMP is used.

Larger images might require it but it's fast enough for me with the for loop
change.

## hw2

In progress.

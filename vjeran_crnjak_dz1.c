#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <ctype.h>

#include <string.h>

#include <assert.h>

#include <sys/types.h>
#include <sys/syscall.h>

#define BIT8LIMIT 255
#define YCBCRFLAG 0
#define RGBFLAG 1
#define DCTFLAG 2
#define PI 3.141592653589793

// 8x8 2d dct
#define W 8

#define max(a, b)                                                              \
  ({                                                                           \
    __typeof__(a) _a = (a);                                                    \
    __typeof__(b) _b = (b);                                                    \
    _a > _b ? _a : _b;                                                         \
  })

#define min(a, b)                                                              \
  ({                                                                           \
    __typeof__(a) _a = (a);                                                    \
    __typeof__(b) _b = (b);                                                    \
    _a < _b ? _a : _b;                                                         \
  })

const float sqrt1div2 = 1.414213562373095048801688724209 * 0.5;
float C(char i) { return i == 0 ? sqrt1div2 : 1.0; }

float Cos[W][W];
void initialize() {
  for (int i = 0; i < W; i++) {
    for (int j = 0; j < W; j++) {
      Cos[i][j] = cos(PI * j * (2 * i + 1) * (1.0 / 16.0));
    }
  }
}

// luminance quantization table
float K1[W][W] = {{16, 11, 10, 16, 24, 40, 51, 61},     //
                  {12, 12, 14, 19, 26, 58, 60, 55},     //
                  {14, 13, 16, 24, 40, 57, 69, 56},     //
                  {14, 17, 22, 29, 51, 87, 80, 62},     //
                  {18, 22, 37, 56, 68, 109, 103, 77},   //
                  {24, 35, 55, 64, 81, 104, 113, 92},   //
                  {49, 64, 78, 87, 103, 121, 120, 101}, //
                  {72, 92, 95, 98, 112, 100, 103, 99}}; //

// chrominance quantization table
float K2[W][W] = {{17, 18, 24, 47, 99, 99, 99, 99},  //
                  {18, 21, 26, 66, 99, 99, 99, 99},  //
                  {24, 26, 56, 99, 99, 99, 99, 99},  //
                  {47, 66, 99, 99, 99, 99, 99, 99},  //
                  {99, 99, 99, 99, 99, 99, 99, 99},  //
                  {99, 99, 99, 99, 99, 99, 99, 99},  //
                  {99, 99, 99, 99, 99, 99, 99, 99},  //
                  {99, 99, 99, 99, 99, 99, 99, 99}}; //

typedef unsigned char byte;
typedef char sbyte; // signed byte

typedef struct {
  byte r;
  byte g;
  byte b;
} rgb;

typedef struct {
  sbyte y;
  sbyte cb;
  sbyte cr;
} yCbCr;

typedef union {
  rgb rgbPx;
  yCbCr yCbCrPx;
  yCbCr dct;
} pixel;

int clip(int minVal, int maxVal, float val) {
  return max(minVal, min(maxVal, roundf(val)));
}
/*
  Y = 0.299 R + 0.587 G + 0.114 B
  Cb = - 0.1687 R - 0.3313 G + 0.5 B + 128
  Cr = 0.5 R - 0.4187 G - 0.0813 B + 128
*/
pixel rgbToYCbCr(pixel px) { // move done here
  rgb a = px.rgbPx;
  pixel b = {{clip(-128, 127, 0.299 * a.r + 0.587 * a.g + 0.114 * a.b - 128.),
              clip(-128, 127, -0.1687 * a.r - 0.3313 * a.g + 0.5 * a.b),
              clip(-128, 127, 0.5 * a.r - 0.4187 * a.g - 0.0813 * a.b)}};
  return b;
}
/*
  R = Y + 1.402 (Cr-128)
  G = Y - 0.34414 (Cb-128) - 0.71414 (Cr-128)
  B = Y + 1.772 (Cb-128)
 */
pixel yCbCrToRgb(pixel px) {
  yCbCr a = px.yCbCrPx;
  pixel b = {{clip(0, 255, a.y + 128. + 1.402 * (a.cr)),
              clip(0, 255, a.y + 128. - 0.34414 * (a.cb) - 0.71414 * (a.cr)),
              clip(0, 255, a.y + 128. + 1.772 * (a.cb))}};
  return b;
}

void assertRound() {
  assert(roundf(1.3) == 1);
  assert(roundf(1.51) == 2);
  assert(roundf(-1.3) == -1);
  assert(roundf(-1.51) == -2);
  assert(roundf(1.5) == 2);
  assert(roundf(-1.5) == -2);
}

typedef struct {
  char magicNumber[3];        // magic number for identifying the file type
  unsigned width;             // width of the image
  unsigned height;            // height of the image
  unsigned short maxColorVal; // maximum color value
  pixel *pixels;              // rgb pixels (8-bit)
  char rory;                  // tells if rgb or ycbcr
} ppm_byte;

pixel *getPixel(ppm_byte *ppm, unsigned i, unsigned j) {
  assert(i < ppm->height && j < ppm->width);
  return &ppm->pixels[i * ppm->width + j];
}
float dctmul(int i, int j, int u, int v, float a, float K) {
  return a * Cos[i][u] * Cos[j][v];
}
float idctmul(int u, int v, int i, int j, float a, float K) {
  return C(u) * C(v) * (a * K) * Cos[i][u] * Cos[j][v];
}
float dctcorrect(int u, int v, float K) { return 1. / 4. * C(u) * C(v) / K; }
float idctcorrect(int u, int v, float K) { return 1. / 4.; }

// general dct, depending on the parameters it is dct or idct
void gdct2d8x8(ppm_byte *ppm, int starti, int startj,
               // dct mul doesn't do anything
               float (*mul)(int, int, int, int, float, float),
               // idct correct multiplies by 1. / 4.
               float (*correct)(int, int, float)) {
  struct {
    float a, b, c;
  } vals[W][W];
  memset(vals, 0, (W * W) * sizeof(vals[0][0]));
  for (int i = 0; i < W; i++) {
    for (int j = 0; j < W; j++) {
      pixel *px = getPixel(ppm, i + starti, j + startj);
      for (int u = 0; u < W; ++u) {
        for (int v = 0; v < W; ++v) {
          vals[u][v].a += mul(i, j, u, v, px->yCbCrPx.y, K1[i][j]);
          vals[u][v].b += mul(i, j, u, v, px->yCbCrPx.cb, K2[i][j]);
          vals[u][v].c += mul(i, j, u, v, px->yCbCrPx.cr, K2[i][j]);
        }
      }
      // printf("%lf %lf %lf\n", vals[u][v].a, vals[u][v].b, vals[u][v].c);
    }
  }
  for (int u = 0; u < W; u++) {
    for (int v = 0; v < W; v++) {
      pixel *px = getPixel(ppm, u + starti, v + startj);
      vals[u][v].a *= correct(u, v, K1[u][v]);
      vals[u][v].b *= correct(u, v, K2[u][v]);
      vals[u][v].c *= correct(u, v, K2[u][v]);
      px->dct.y = clip(-128, 127, vals[u][v].a);
      px->dct.cb = clip(-128, 127, vals[u][v].b);
      px->dct.cr = clip(-128, 127, vals[u][v].c);
    }
  }
}

void gdct2d(ppm_byte *ppm, float (*mul)(int, int, int, int, float, float),
            float (*correct)(int, int, float)) {
  for (int startX = 0; startX < ppm->height; startX += W) {
    for (int startY = 0; startY < ppm->width; startY += W) {
      gdct2d8x8(ppm, startX, startY, mul, correct);
    }
  }
}

void dct2d(ppm_byte *ppm) {
  gdct2d(ppm, dctmul, dctcorrect);
  ppm->rory = DCTFLAG;
}

void idct2d(ppm_byte *ppm) {
  gdct2d(ppm, idctmul, idctcorrect);
  ppm->rory = YCBCRFLAG;
}

ppm_byte readPPMImage1Byte(FILE *fp, char *magicNumber, unsigned width,
                           unsigned height, unsigned short maxColorVal) {
  ppm_byte image;
  strncpy(image.magicNumber, magicNumber, sizeof(char) * 3);
  image.width = width;
  image.height = height;
  image.maxColorVal = maxColorVal;
  image.rory = RGBFLAG;

  size_t numOfPixels = width * height;
  image.pixels = (pixel *)malloc(numOfPixels * sizeof(pixel));
  // reads the whole image into pixels array
  fread(image.pixels, sizeof(pixel), numOfPixels, fp);

  return image;
}

void writePPMImage1Byte(FILE *fp, ppm_byte *ppm) {
  fprintf(fp, "%s\n%u %u\n%u\n", ppm->magicNumber, ppm->width, ppm->height,
          ppm->maxColorVal);
  fwrite(ppm->pixels, sizeof(pixel), ppm->width * ppm->height, fp);
}

void convertToYCBCR(ppm_byte *ppm) {
  assert(ppm->rory == RGBFLAG);
  for (size_t i = 0, len = ppm->height * ppm->width; i < len; ++i) {
    ppm->pixels[i] = rgbToYCbCr(ppm->pixels[i]);
  }
  ppm->rory = YCBCRFLAG;
}

void convertToRGB(ppm_byte *ppm) {
  assert(ppm->rory == YCBCRFLAG);
  for (size_t i = 0, len = ppm->height * ppm->width; i < len; ++i) {
    ppm->pixels[i] = yCbCrToRgb(ppm->pixels[i]);
  }
  ppm->rory = RGBFLAG;
}

void printPPMInText(ppm_byte *ppm) {
  printf("MN: %2s W: %u H: %u MCV: %u\n", ppm->magicNumber, ppm->width,
         ppm->height, ppm->maxColorVal);
  for (size_t i = 0, n = ppm->height; i < n; ++i) {
    for (size_t j = 0, m = ppm->width; j < m; ++j) {
      pixel *px = getPixel(ppm, i, j);
      printf("P[%lu,%lu] = %u %u %u\n", i, j, px->rgbPx.r, px->rgbPx.g,
             px->rgbPx.b);
    }
  }
}

int isCorrect(char magicNumber[2]) {
  return magicNumber[0] == 'P' && magicNumber[1] == '6';
}

void fprintfCoeff(ppm_byte *ppm, int blockRow, int blockCol, FILE *fp,
                  sbyte (*get)(pixel *)) {
  for (int i = blockRow, n = i + W; i < n; ++i) {
    for (int j = blockCol, m = j + W; j < m; ++j) {
      pixel *px = getPixel(ppm, i, j);
      fprintf(fp, "%d", get(px));
      if (!(j + 1 == m)) {
        fprintf(fp, " ");
      }
    }
    fprintf(fp, "\n");
  }
}

sbyte getY(pixel *px) { return px->dct.y; }
sbyte getCb(pixel *px) { return px->dct.cb; }
sbyte getCr(pixel *px) { return px->dct.cr; }

void writeCoefficients(ppm_byte *ppm, int block, FILE *fp) {
  int blockRow = ((block * W) / ppm->width) * W;
  int blockCol = ((block * W) % ppm->width);
  fprintfCoeff(ppm, blockRow, blockCol, fp, getY);
  fprintf(fp, "\n");
  fprintfCoeff(ppm, blockRow, blockCol, fp, getCb);
  fprintf(fp, "\n");
  fprintfCoeff(ppm, blockRow, blockCol, fp, getCr);
}

int main(int argc, char const *argv[]) {
  setbuf(stdout, NULL);
  assertRound();
  initialize();
  if (argc < 4) {
    perror("Too few args. Run using ./exe pic.ppm NUM out.txt");
    return EXIT_FAILURE;
  }

  FILE *fp = fopen(argv[1], "r");
  if (!fp) {
    perror("File opening failed.");
    return EXIT_FAILURE;
  }

  char magicNumber[3] = {0, 0, 0};
  unsigned width, height;
  unsigned maxColorVal;

  fscanf(fp, " %2c %u %u %u", magicNumber, &width, &height, &maxColorVal);
  printf("MN: %s W: %u H: %u MCV: %u\n", magicNumber, width, height,
         maxColorVal);
  if (!isCorrect(magicNumber)) {
    perror("Magic number of the file isn't equal \"P6\". Not supported.");
    return EXIT_FAILURE;
  }

  if (!isspace(fgetc(fp))) {
    perror("Whitespace character should have followed.");
    return EXIT_FAILURE;
  }

  if (maxColorVal <= BIT8LIMIT) {
    char wholeImage = argc >= 5;
    ppm_byte image =
        readPPMImage1Byte(fp, magicNumber, width, height, maxColorVal);
    // printPPMInText(&image);
    FILE *outf = fopen(argv[3], "w+");
    if (!outf) {
      perror("File opening failed.");
      return EXIT_FAILURE;
    }
    printf("Converting from RGB to YCbCr... ");
    convertToYCBCR(&image);
    printf("done.\n");
    int block = atoi(argv[2]);

    if (block >= ((image.width * image.height) / (W * W))) {
      perror("Block number invalid.");
      return EXIT_FAILURE;
    }
    printf("2D discrete cosine transform... ");
    if (!wholeImage) {
      int blockRow = ((block * W) / image.width) * W;
      int blockCol = ((block * W) % image.width);
      gdct2d8x8(&image, blockRow, blockCol, dctmul, dctcorrect);
    } else {
      dct2d(&image);
    }
    printf("done.\n");

    if (!wholeImage) {
      printf("Writing coefficients in output file... ");
      writeCoefficients(&image, block, outf);
      printf("done.\n");
    } else {
      printf("Inverse 2D discrete cosine transform... ");
      idct2d(&image);
      printf("done.\n");

      printf("Converting from YCbCr to RGB... ");
      convertToRGB(&image);
      printf("done.\n");

      printf("Writing out image... ");
      writePPMImage1Byte(outf, &image);
      printf("done.\n");
    }
    free(image.pixels);

  } else {
    perror("Maximum color value larger than 255 isn't supported.");
    return EXIT_FAILURE;
  }
  return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <ctype.h>

#include <string.h>

#include <assert.h>
#include <limits.h>

#include <sys/types.h>
#include <sys/syscall.h>

#define BIT8LIMIT 255

// 16x16 reference block
#define W 16
// -16 to 16 search window
#define SW 16

#define max(a, b)                                                              \
  ({                                                                           \
    __typeof__(a) _a = (a);                                                    \
    __typeof__(b) _b = (b);                                                    \
    _a > _b ? _a : _b;                                                         \
  })

#define min(a, b)                                                              \
  ({                                                                           \
    __typeof__(a) _a = (a);                                                    \
    __typeof__(b) _b = (b);                                                    \
    _a < _b ? _a : _b;                                                         \
  })

typedef unsigned char byte;

typedef struct { byte c; } pixel;

typedef struct {
  char magicNumber[3];        // magic number for identifying the file type
  unsigned width;             // width of the image
  unsigned height;            // height of the image
  unsigned short maxColorVal; // maximum color value
  pixel *pixels;              // grayscale pixels (8-bit)
} pgm_byte;

pixel getPixel(pgm_byte *ppm, unsigned i, unsigned j) {
  assert(i < ppm->height && j < ppm->width);
  return ppm->pixels[i * ppm->width + j];
}

void writePPMImage1Byte(FILE *fp, pgm_byte *ppm) {
  fprintf(fp, "%s\n%u %u\n%u\n", ppm->magicNumber, ppm->width, ppm->height,
          ppm->maxColorVal);
  fwrite(ppm->pixels, sizeof(pixel), ppm->width * ppm->height, fp);
}

void printPPMInText(pgm_byte *ppm) {
  printf("MN: %2s W: %u H: %u MCV: %u\n", ppm->magicNumber, ppm->width,
         ppm->height, ppm->maxColorVal);
  for (size_t i = 0, n = ppm->height; i < n; ++i) {
    for (size_t j = 0, m = ppm->width; j < m; ++j) {
      pixel px = getPixel(ppm, i, j);
      printf("P[%lu,%lu] = %u\n", i, j, px.c);
    }
  }
}

int isCorrect(char magicNumber[2]) {
  return magicNumber[0] == 'P' && magicNumber[1] == '5';
}

unsigned absdiff(byte a, byte b) { return a > b ? (a - b) : (b - a); }

unsigned findDiff(pgm_byte *prev, pgm_byte *cur, int px, int py, int cx,
                  int cy) {
  unsigned diff = 0;
  for (int x = 0; x < W; ++x) {
    for (int y = 0; y < W; ++y) {
      diff += absdiff(getPixel(prev, px + x, py + y).c,
                      getPixel(cur, cx + x, cy + y).c);
    }
  }
  return diff;
}

void findClosestMove(pgm_byte *prev, pgm_byte *cur, int block, int *sx,
                     int *sy) {
  int X = ((block * W) / prev->width) * W;
  int Y = ((block * W) % prev->width);

  int xstart = max(0, X - SW);
  int xend = min(prev->height - W, X + SW + 1); // +1 for -SW to SW shift
  int ystart = max(0, Y - SW);
  int yend = min(prev->width - W, Y + SW + 1);
  int mx, my;
  unsigned mindiff = 10000000;
  for (int x = xstart; x < xend; ++x) {
    for (int y = ystart; y < yend; ++y) {
      unsigned diff = findDiff(prev, cur, X, Y, x, y);
      if (diff < mindiff) {
        mx = x - X;
        my = y - Y;
        mindiff = diff;
      }
    }
  }
  *sx = my; // input behaves in left and right, y is left right (cols)
  *sy = mx; // input behaves in down and up, x is down up (rows)
}

pgm_byte readPPMImage1Byte(FILE *fp) {

  char magicNumber[3] = {0, 0, 0};
  unsigned width, height;
  unsigned maxColorVal;

  fscanf(fp, " %2c %u %u %u", magicNumber, &width, &height, &maxColorVal);
  // printf("MN: %s W: %u H: %u MCV: %u\n", magicNumber, width, height,
  //        maxColorVal);
  if (!isCorrect(magicNumber)) {
    perror("Magic number of the file isn't equal \"P5\". Not supported");
    exit(EXIT_FAILURE);
  }

  if (maxColorVal > BIT8LIMIT) {
    perror("Max color value larger than 255 isn't supported");
    exit(EXIT_FAILURE);
  }

  if (!isspace(fgetc(fp))) {
    perror("Whitespace character should have followed");
    exit(EXIT_FAILURE);
  }
  pgm_byte image;
  strncpy(image.magicNumber, magicNumber, sizeof(char) * 3);
  image.width = width;
  image.height = height;
  image.maxColorVal = maxColorVal;

  size_t numOfPixels = width * height;
  image.pixels = (pixel *)malloc(numOfPixels * sizeof(pixel));
  // reads the whole image into pixels array
  fread(image.pixels, sizeof(pixel), numOfPixels, fp);

  return image;
}

int main(int argc, char const *argv[]) {
  setbuf(stdout, NULL);
  if (argc < 1) {
    perror("Too few args. Run using ./exe BLOCKNUM");
    return EXIT_FAILURE;
  }

  FILE *fp = fopen("lenna.pgm", "r");
  if (!fp) {
    perror("File opening failed");
    return EXIT_FAILURE;
  }

  pgm_byte previousImage = readPPMImage1Byte(fp);

  fp = fopen("lenna1.pgm", "r");
  if (!fp) {
    perror("File opening failed");
    return EXIT_FAILURE;
  }
  pgm_byte currentImage = readPPMImage1Byte(fp);

  int block = atoi(argv[1]);

  if (block >= ((previousImage.width * previousImage.height) / (W * W))) {
    perror("Block number invalid");
    return EXIT_FAILURE;
  }

  if ((previousImage.width != currentImage.width) ||
      (previousImage.height != currentImage.height)) {
    perror("Images not of same dimensions. Can't compare");
    return EXIT_FAILURE;
  }

  int sx, sy;
  findClosestMove(&previousImage, &currentImage, block, &sx, &sy);
  printf("(%d, %d)\n", sx, sy);
  free(previousImage.pixels);
  free(currentImage.pixels);

  return 0;
}
